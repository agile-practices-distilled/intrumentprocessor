using System;

namespace InstrumentProcessor
{
    public interface IInstrument
    {
        void Execute(string task);
        event EventHandler Finished;
        event EventHandler Error;
    }

    public class TaskFinishedEventArgs : EventArgs
    {
        public string Task { get; }

        public TaskFinishedEventArgs(string task)
        {
            Task = task;
        }
    }
}