﻿using System;

namespace InstrumentProcessor
{
    public class InstrumentProcessor : IInstrumentProcessor
    {
        private readonly ITaskDispatcher _taskDispatcher;
        private readonly IInstrument _instrument;
        private readonly IConsoleWriter _consoleWriter;

        public InstrumentProcessor(
            ITaskDispatcher taskDispatcher, 
            IInstrument instrument, 
            IConsoleWriter consoleWriter)
        {
            _taskDispatcher = taskDispatcher;
            _instrument = instrument;
            _consoleWriter = consoleWriter;

            _instrument.Error += (sender, args) => _consoleWriter.Write("Error occurred");;
            _instrument.Finished += (sender, args) => _taskDispatcher.FinishedTask(((TaskFinishedEventArgs)args).Task);
        }

        public void Process()
        {
            var task = _taskDispatcher.GetTask();
            
            _instrument.Execute(task);
        }
    }
}