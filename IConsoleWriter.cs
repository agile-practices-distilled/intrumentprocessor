namespace InstrumentProcessor
{
    public interface IConsoleWriter
    {
        void Write(string text);
    }
}