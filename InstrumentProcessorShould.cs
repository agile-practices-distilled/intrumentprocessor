using System;
using Xunit;
using Moq;

namespace InstrumentProcessor
{
    public class InstrumentProcessorShould
    {
        private readonly InstrumentProcessor _instrumentProcessor;
        
        private readonly Mock<ITaskDispatcher> _taskDispatcherMock = new Mock<ITaskDispatcher>();
        private readonly Mock<IInstrument> _instrumentMock = new Mock<IInstrument>();
        private readonly Mock<IConsoleWriter> _consoleWriterMock = new Mock<IConsoleWriter>();
        
        private const string A_TASK = "a task";

        public InstrumentProcessorShould()
        {
            _instrumentProcessor = new InstrumentProcessor(
                _taskDispatcherMock.Object, 
                _instrumentMock.Object,
                _consoleWriterMock.Object);
        }
        
        [Fact]
        public void get_next_task_when_starts_processing()
        {
            _taskDispatcherMock.Setup(t => t.GetTask()).Returns(A_TASK);
            
            _instrumentProcessor.Process();
            
            _instrumentMock.Verify(i => i.Execute(A_TASK));
        }
        
        [Fact]
        public void output_error_in_console_if_error_occurs_in_instrument()
        {
            _instrumentMock.Raise(i => i.Error += null, new EventArgs());
            
            _consoleWriterMock.Verify(i => i.Write("Error occurred"));
        }
        
        [Fact]
        public void pass_exception_to_the_caller_when_instrument_throws()
        {
            _taskDispatcherMock.Setup(t => t.GetTask()).Returns((string)null);
            _instrumentMock.Setup(i => i.Execute(null)).Throws<ArgumentNullException>();
            
            Assert.Throws<ArgumentNullException>(() => _instrumentProcessor.Process());
        }
        
        
        [Fact]
        public void call_the_finished_task_when_task_finished()
        {
            _instrumentMock.Raise(i => i.Finished += null, new TaskFinishedEventArgs(A_TASK));
            
            _taskDispatcherMock.Verify(t => t.FinishedTask(A_TASK));
        }
    }
}